# MSI GE60 2OE hack

Настройки и файлы для ноутбука MSI GE60 2OE. 
Версия Mac OS 10.12


### Драйверы

* AppleImageCodec.efi
* AppleKeyAggregator.efi
* AppleUITheme.efi
* AptioMemoryFix.efi 
* AudioDxe.efi
* CsmVideoDxe.efi
* DataHubDxe.efi
* Fat.efi
* FirmwareVolume.efi
* FSInject.efi
* HFSPlus.efi
* OsxFatBinaryDrv.efi
* PartitionDxe.efi
* VirtualSmc.efi

### Kexts
* AirportBrcmFixup.kext v2.0.4
* AppleACPIPS2Nub.kext 
* AppleALC.kext v1.4.4
* AppleIntelKBLGraphicsFramebufferInjector_3e9x.kext v0.5.0
* ApplePS2Controller.kext v1.1.5
* AtherosE2200Ethernet.kext v2.2.2
* CodecCommander.kext v2.7.1
* FakePCIID_Broadcom_WiFi.kext v1.3.15
* FakePCIID_Intel_HD_Graphics.kext 1.3.15
* FakePCIID_Intel_HDMI_Audio.kext v1.3.15
* FakePCIID.kext v1.3.15
* Lilu.kext v1.4.0
* SMCBatteryManager.kext v1.0
* SMCLightSensor.kext v1.0
* SMCProcessor.kext v1.0.9
* SMCSuperIO.kext v1.0.9
* USBInjectAll.kext v0.7.1
* VirtualSMC.kext v1.0.9
* WhateverGreen.kext v1.3.5
